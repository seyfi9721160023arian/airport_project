import psycopg2

conn = psycopg2.connect(
    host="localhost",
    port="5432",
    database="",
    user="postgres",
    password="as921990")

cur = conn.cursor()

if __name__ == "__main__":
  choose = int( input("""
  0. Exit
  1. Profile of all employees
  2. Employee details with national number 127
  3. The name of the employee or employees who receive the highest salary
  4. The name of the employee or employees who receive the lowest salary
  5. Names of employees with more than 5 years of work experience
  6. Names of all female employees
  7. Names of all male employees
  8. Profile of employees born before 1990
  9. Profile of the oldest employee
  10. Profile of the youngest employee
  11. Information on all trips today
  12. Travel information to London
  13. Travel information to Paris
  14. Travel information to London and Paris
  15. Travel information to London at 8 p.m.
  16. Name of travel travelers to number 1447
  17. Number of aircraft manufactured by Qatar Airways
  18. Information on aircraft with a capacity of more than 300
  19. Information on aircraft with a capacity between 100 and 200
  20. Information on aircraft with a capacity of less than 100
  21. Information on the aircraft with the highest capacity
  22. Information on aircraft with the lowest capacity
  23. Names of all pilots
  24. insert Plane info
  25. insert Travel info
  26. insert Passenger info
  27. insert Pilot info
  28. insert Flight Attendant info
  """) )

while(choose != 0) :
  if choose == 1 :
    cur.execute('select * from employee join person using(national_Code);')
  elif choose == 2 :
    cur.execute('select * from employee join person using(national_Code) where national_code = "127";')
  elif choose == 3 :
    cur.execute('select fname + lname as name from emplyee join person using(national_Code) where salary >= All(select salary from employee);')
  elif choose == 4 :
    cur.execute('select fname + lname as name from emplyee join person using(national_Code) where salary <= All(select salary from employee);')
  elif choose == 5 :
    cur.execute('select * from employee where work_exprience > 5;')
  elif choose == 6 :
    cur.execute('select fname + lname as name from emplyee join person using(national_Code) where gender = "woman"')
  elif choose == 7 :
    cur.execute('select fname + lname as name from emplyee join person using(national_Code) where gender = "man"')
  elif choose == 8 :
    cur.execute('select * from emplyee join person using(national_Code) where birthday <= '1990'')
  elif choose == 9 :
    cur.execute('select * from emplyee as emp1 join person using(national_Code) where not exist(select * from employee as emp2 join person using(national_Code) where emp1.national_code <> em2.national_code and emp2.birthday > em1.birthday)')
  elif choose == 10 :
    cur.execute('select * from emplyee as emp1 join person using(national_Code) where not exist(select * from employee as emp2 join person using(national_Code) where emp1.national_code <> em2.national_code and emp2.birthday < em1.birthday)')
  elif choose == 11 :
    cur.execute('select * from travel where date = Date.now()')
  elif choose == 12 :
    cur.execute('select * from travel where beginning = "london"')
  elif choose == 13 :
    cur.execute('select * from travel where destination = "paris"')
  elif choose == 14 :
    cur.execute('select * from travel where beginning = "london" and destination = "paris"')
  elif choose == 15 :
    cur.execute('select * from travel where beginning = "london" and time = "20:00"')
  elif choose == 16 :
    cur.execute('select fname + lname as name from pass_trav join travel using(travel_id) join passenger on passenger_id = national_code join person using(national_code) where travel_id = 1447')
  elif choose == 17 :
    cur.execute('select plane_id from plane where company = "QatarAir"')
  elif choose == 18 :
    cur.execute('select * from plane where capacity > 300')
  elif choose == 19 :
    cur.execute('select * from plane where capacity between 100 and 200')
  elif choose == 20 :
    cur.execute('select * from plane where capacity < 100')
  elif choose == 21 :
    cur.execute('select * from plane where capacity >= All(select capaticy from plane)')
  elif choose == 22 :
    cur.execute('select * from plane where capacity <= All(select capaticy from plane)')
  elif choose == 23 :
    cur.execute('select fname + lname as name from pilot join person using(national_Code)')
  elif choose == 24 :
    id_ = input("enter plane's ID: ")
    company = input("enter plane's Company: ")
    passenger_capacity = input("enter plane's Passenger Capacity: ")
    year_of_construction = input("enter plane's Year of construction: ")
    fuel_capacity = input("enter plane's Fuel capacity: ")
    cur.execute('INSERT INTO plane(ID,company,passenger_capacity,year_of_construction,fuel_capacity) VALUES(%s,%s,%s,%s,%s)',(id_,company,passenger_capacity,year_of_construction,fuel_capacity)')
  elif choose == 25 :
    id_ = input("enter travel's ID: ")
    date = input("enter travel's Date: ")
    time = input("enter travel's time: ")
    beginning = input("enter travel's beginning: ")
    destination = input("enter travel's destination: ")
    cur.execute('INSERT INTO Travel(ID,date,time,beginning,destination) VALUES(%s,%s,%s,%s,%s)',(id_,date,time,beginning,destination)')
  elif choose == 26 :
    national_code = input("enter national code: ")
    fname = input("enter first name: ")
    lname = input("enter last name: ")
    gender = input("enter gender: ")
    birthday = input("enter birthday: ")
    phone_number = input("enter phone number: ")

    credit = input("enter credit: ")
    health_status = input("enter health status: ")
    cur.execute('INSERT INTO person(national_code,fname,lname,gender,birthday,phone_number) VALUES(%s,%s,%s,%s,%s,%s)',(national_code,fname,lname,gender,birthday,phone_number)')
    cur.execute('INSERT INTO passenger(national_code,credit,health_status) VALUES(%s,%s,%s)',(credit,health_status)')
  elif choose == 27 :
    national_code = input("enter national code: ")
    fname = input("enter first name: ")
    lname = input("enter last name: ")
    gender = input("enter gender: ")
    birthday = input("enter birthday: ")
    phone_number = input("enter phone number: ")

    salary = input("enter salary: ")
    work_experience = input("enter work experience: ")
    role = input("enter role: ")

    level = input("enter level: ")
    eye_health = input("enter eye health: ")
    cur.execute('INSERT INTO person(national_code,fname,lname,gender,birthday,phone_number) VALUES(%s,%s,%s,%s,%s,%s)',(national_code,fname,lname,gender,birthday,phone_number)')
    cur.execute('INSERT INTO employee(national_code,salary,work_experience,role) VALUES(%s,%s,%s,%s)',(salary,work_experience,role)')
    cur.execute('INSERT INTO pilot(national_code,level,eye_health) VALUES(%s,%s,%s)',(level,eye_health)')
  
  elif choose == 28 :
    national_code = input("enter national code: ")
    fname = input("enter first name: ")
    lname = input("enter last name: ")
    gender = input("enter gender: ")
    birthday = input("enter birthday: ")
    phone_number = input("enter phone number: ")

    salary = input("enter salary: ")
    work_experience = input("enter work experience: ")
    role = input("enter role: ")

    height = input("enter height: ")
    weight = input("enter weight: ")
    teeth_health = input("enter teeth health: ")
    cur.execute('INSERT INTO person(national_code,fname,lname,gender,birthday,phone_number) VALUES(%s,%s,%s,%s,%s,%s)',(national_code,fname,lname,gender,birthday,phone_number)')
    cur.execute('INSERT INTO employee(national_code,salary,work_experience,role) VALUES(%s,%s,%s,%s)',(salary,work_experience,role)')
    cur.execute('INSERT INTO flight Attendant(national_code,height,weight,teeth_health) VALUES(%s,%s,%s,%s)',(height,weight,teeth_health)')
  else:
    print('wrong input, try again')