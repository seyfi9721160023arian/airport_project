import psycopg2

conn = psycopg2.connect(
    host="localhost",
    port="5432",
    database="",
    user="postgres",
    password="as921990")

cur = conn.cursor()

if __name__ == "__main__":
  choose = int( input("""
  0. Exit
  1. Names of pilots with level 3
  2. Names of pilots with levels between 1 and 3
  3. Names of pilots with level 2 and salaries of more than 10 million
  4. Names of pilots with the role of the first pilot
  5. Names of pilots with the role of co-pilot
  6. Names and dental health status of all stewards
  7. Height and weight of all hostesses
  8. Employee information whose national number starts with 127
  9. Names of employees with salaries between 5 and 8 million
  10. Names of pilots with level 2 or 4
  11. Identification and number of times each aircraft has been used in the voyage
  12. Identification and number of times aircraft with at least 100 trips have been used in the voyage
  13. Identification and company of aircraft that have been on less than 10 trips
  14. Identification and number of times that Qatar Airways planes have traveled
  15. Information on trips to history that have begun today
  16. Number of all employees
  17. Number of passengers born 2000 onwards
  18. Number of aircraft manufactured by Qatar Company
  19. Total salary of workers
  20. Number of female passengers
  21. Number of male passengers
  22. Information of all passengers sorted by family
  23. Most recorded work experience among employees
  24. insert Plane info
  25. insert Travel info
  26. insert Passenger info
  27. insert Pilot info
  28. insert Flight Attendant info
  """) )

while(choose != 0) :
  if choose == 1 :
    cur.execute('select fname + lname as name from pilot join person using(national_Code) where level = 3')
  elif choose == 2 :
    cur.execute('select fname + lname as name from pilot join person using(national_Code) where level between 1 and 3')
  elif choose == 3 :
    cur.execute('select fname + lname as name from pilot join employee using(national_Code) join person using(national_Code) where level = 2 and salary > 10000000')
  elif choose == 4 :
    cur.execute('select fname + lname as name from pilot join employee using(national_Code) join person using(national_Code) where role = "primary"')
  elif choose == 5 :
    cur.execute('select fname + lname as name from pilot join employee using(national_Code) join person using(national_Code) where role = "assistance"')
  elif choose == 6 :
    cur.execute('select fname + lname as name,teeth_health from attendant join person using(national_Code)')
  elif choose == 7 :
    cur.execute('height,weight as name from attendant join person using(national_Code) where gender = "woman"')
  elif choose == 8 :
    cur.execute('select * from employee join person using(national_Code) where national_code like "127%";')
  elif choose == 9 :
    cur.execute('select fname + lname as name from emplyee join person using(national_Code) where salary between 5000000 and 8000000;')
  elif choose == 10 :
    cur.execute('select fname + lname as name from pilot join person using(national_Code) where level in (2 , 4)')
  elif choose == 11 :
    cur.execute('select plane_id, count(travel_id) from plane join plane_trav using(plane_id) group by plane_id')
  elif choose == 12 :
    cur.execute('select plane_id, count(travel_id) from plane join plane_trav using(plane_id) group by plane_id having count(travel_id) >= 100')
  elif choose == 13 :
    cur.execute('select plane_id, company from plane join plane_trav using(plane_id) group by plane_id having count(travel_id) < 10')
  elif choose == 14 :
    cur.execute('select plane_id, count(travel_id) from plane join plane_trav using(plane_id) where company = "QatarAir" group by plane_id')
  elif choose == 15 :
    cur.execute('select * from travel where date = Date.now() and time < Time.now()')
  elif choose == 16 :
    cur.execute('select count(*) from employee')
  elif choose == 17 :
    cur.execute('select count(*) from passenger join person using(national_code) where birthday >= '2000'')
  elif choose == 18 :
    cur.execute('select count(*) from plane where company = "QatarAir"')
  elif choose == 19 :
    cur.execute('select sum(salary) from employee')
  elif choose == 20 :
    cur.execute('select count(*) from passenger join person using(national_code) where gender = "woman"')
  elif choose == 21 :
    cur.execute('select count(*) from passenger join person using(national_code) where gender = "man"')
  elif choose == 22 :
    cur.execute('select * from passenger join person using(national_code) order by lname')
  elif choose == 23 :
    cur.execute('select max(work_experience) from employee')
  elif choose == 24 :
    id_ = input("enter plane's ID: ")
    company = input("enter plane's Company: ")
    passenger_capacity = input("enter plane's Passenger Capacity: ")
    year_of_construction = input("enter plane's Year of construction: ")
    fuel_capacity = input("enter plane's Fuel capacity: ")
    cur.execute('INSERT INTO plane(ID,company,passenger_capacity,year_of_construction,fuel_capacity) VALUES(%s,%s,%s,%s,%s)',(id_,company,passenger_capacity,year_of_construction,fuel_capacity)')
  elif choose == 25 :
    id_ = input("enter travel's ID: ")
    date = input("enter travel's Date: ")
    time = input("enter travel's time: ")
    beginning = input("enter travel's beginning: ")
    destination = input("enter travel's destination: ")
    cur.execute('INSERT INTO Travel(ID,date,time,beginning,destination) VALUES(%s,%s,%s,%s,%s)',(id_,date,time,beginning,destination)')
  elif choose == 26 :
    national_code = input("enter national code: ")
    fname = input("enter first name: ")
    lname = input("enter last name: ")
    gender = input("enter gender: ")
    birthday = input("enter birthday: ")
    phone_number = input("enter phone number: ")

    credit = input("enter credit: ")
    health_status = input("enter health status: ")
    cur.execute('INSERT INTO person(national_code,fname,lname,gender,birthday,phone_number) VALUES(%s,%s,%s,%s,%s,%s)',(national_code,fname,lname,gender,birthday,phone_number)')
    cur.execute('INSERT INTO passenger(national_code,credit,health_status) VALUES(%s,%s,%s)',(credit,health_status)')
  elif choose == 27 :
    national_code = input("enter national code: ")
    fname = input("enter first name: ")
    lname = input("enter last name: ")
    gender = input("enter gender: ")
    birthday = input("enter birthday: ")
    phone_number = input("enter phone number: ")

    salary = input("enter salary: ")
    work_experience = input("enter work experience: ")
    role = input("enter role: ")

    level = input("enter level: ")
    eye_health = input("enter eye health: ")
    cur.execute('INSERT INTO person(national_code,fname,lname,gender,birthday,phone_number) VALUES(%s,%s,%s,%s,%s,%s)',(national_code,fname,lname,gender,birthday,phone_number)')
    cur.execute('INSERT INTO employee(national_code,salary,work_experience,role) VALUES(%s,%s,%s,%s)',(salary,work_experience,role)')
    cur.execute('INSERT INTO pilot(national_code,level,eye_health) VALUES(%s,%s,%s)',(level,eye_health)')
  
  elif choose == 28 :
    national_code = input("enter national code: ")
    fname = input("enter first name: ")
    lname = input("enter last name: ")
    gender = input("enter gender: ")
    birthday = input("enter birthday: ")
    phone_number = input("enter phone number: ")

    salary = input("enter salary: ")
    work_experience = input("enter work experience: ")
    role = input("enter role: ")

    height = input("enter height: ")
    weight = input("enter weight: ")
    teeth_health = input("enter teeth health: ")
    cur.execute('INSERT INTO person(national_code,fname,lname,gender,birthday,phone_number) VALUES(%s,%s,%s,%s,%s,%s)',(national_code,fname,lname,gender,birthday,phone_number)')
    cur.execute('INSERT INTO employee(national_code,salary,work_experience,role) VALUES(%s,%s,%s,%s)',(salary,work_experience,role)')
    cur.execute('INSERT INTO flight Attendant(national_code,height,weight,teeth_health) VALUES(%s,%s,%s,%s)',(height,weight,teeth_health)')
  else:
    print('wrong input, try again')